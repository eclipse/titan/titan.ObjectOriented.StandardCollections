/******************************************************************************
* Copyright (c) 2000-2025 Ericsson Telecom AB
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
*
* Contributors:
*   Lenard, Nagy – initial implementation
*   Elemer, Lelik
*
******************************************************************************/
module OODataStructuresCommon {

import from StorableModule all;

public type class Node {
  var object data;
  var Node prev := null;
  var Node next := null;

  create(in object data := null) {
    this.data := data;
  }

  public function getData() return object {
    return data;
  }

  public function setData(in object data) {
    this.data := data
  }

  public function getPrev() return Node {
    return prev;
  }

  public function setPrev(in Node prev) {
    this.prev := prev
  }

  public function getNext() return Node {
    return next;
  }

  public function setNext(in Node next) {
    this.next := next
  }

}

public type class TreeNode {
  var Storable data;
  var TreeNode parent := null;
  var TreeNode less   := null;
  var TreeNode more   := null;

  create(in Storable data := null) {
    this.data := data;
  }

  public function getData() return Storable {
    return data;
  }

  public function setData(in Storable data) {
    this.data := data
  }

  public function getParent() return TreeNode {
    return parent;
  }

  public function setParent(in TreeNode parent) {
    this.parent := parent
  }

  public function getLess() return TreeNode {
    return less;
  }

  public function setLess(in TreeNode less) {
    this.less := less
  }

  public function getMore() return TreeNode {
    return more;
  }

  public function setMore(in TreeNode more) {
    this.more := more
  }

}

}
