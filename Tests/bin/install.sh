#!/bin/bash

rm -fr ./*.ttcn
#rm -fr ./Makefile

ln -s ../../DataTypes/*.ttcn ./
ln -s ../../StandardCollections/*.ttcn ./
ln -s ../../DataStructures/*.ttcn ./
ln -s ../*.ttcn ./

#ln -s ../Makefile ./
#ln -s ../OOP.cfg ./
#makefilegen -f -k ./*.ttcn
