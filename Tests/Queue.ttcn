/******************************************************************************
* Copyright (c) 2000-2025 Ericsson Telecom AB
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
*
* Contributors:
*   Lenard, Nagy – initial implementation
*   Elemer, Lelik
*
******************************************************************************/
module Queue {

import from TTCN3Queue all;
import from OODataTypes all;
import from TTCN3_standard_collections all;

modulepar {

  integer tsp_index:=999999;

}

type component GeneralComp {
}


testcase tcQueue() runs on GeneralComp {

  timer clock:=3600.0;
  var UniversalCharstring uc0, uc1, uc2;

  log("Create empty queue");
  var TitanQueue q := TitanQueue.create();
  log(q.toString());
  log("Check for isEmpty and size == 0");
  if (q.size() == 0) {
    log("Queue is empty");
    setverdict(pass);
  } else {
    log("Queue not empty");
    setverdict(fail);
  }

  if (q.isEmpty()) {
    log("Queue is confirmed empty");
    setverdict(pass);
  } else {
    log("Queue not confirmed empty");
    setverdict(fail);
  }

  log("Peek at the first  element in  the Queue")     
  log ("Peeking at the first element: ", q.peek());

  log("Trying to remove  one element from an empty Queue")
  log ("First element removed from an empty queue: ", q.remove());      

  var integer s := q.size();
  log ("List size: ", s)
  if (s == 0) {
    setverdict(pass);
  } else {
    setverdict(fail);
  }

  log("Add one element to the Queue");
  uc0 :=UniversalCharstring.create("FirstElement")
  q.add(uc0);
  log(q.toString());
  s := q.size();
  log ("Queue size: ", s)
  if (s == 1) {
    setverdict(pass);
  } else {
    setverdict(fail);
  }

  log("Peek at the first  element in  the Queue")     
  log ("Peeking at the first element: ", q.peek() );

  log("Remove the only element from the Queue")
  uc1 := q.remove() => UniversalCharstring;
  if (uc1.Equals(UniversalCharstring.create("FirstElement"))) {
    setverdict(pass);
  } else {
    log ("Unexpected string returned from Queue: ", uc1.toString())
    setverdict(fail);
  }

  s := q.size();
  log ("Queue size: ", s)
  if (s == 0) {
    setverdict(pass);
  } else {
    setverdict(fail);
  }

  log("Add two elements to Queue");
  q.add(uc1);
  q.add(UniversalCharstring.create("SecondElement"));

  log(q.toString());
  s := q.size();
  log ("Queue size: ", s)
  if (s == 2) {
    setverdict(pass);
  } else {
    setverdict(fail);
  }
  log("Remove two elements from the Queue")
  log ("First element: ", q.remove());
  log(q.toString());
  log ("Second element: ", q.remove());
  log(q.toString());

  s := q.size();
  log ("Queue size: ", s)
  if (s == 0) {
    setverdict(pass);
  } else {
    setverdict(fail);
  }

  log("Add three elements to Queue");
  q.add(uc1);
  log(q.toString());
  q.add(UniversalCharstring.create("SecondElement"));
  log(q.toString());
  q.add(UniversalCharstring.create("ThirdElement"));
  log(q.toString());
  log(q.toString());
  s := q.size();
  log ("List size: ", s)
  if (s == 3) {
    setverdict(pass);
  } else {
    setverdict(fail);
  }


  if(q.contains(uc0))
  {  log("FirstElement is contained")
    setverdict(pass);
  } else {
    log("FirstElement is  not contained")
    setverdict(fail);
  }


  log("Peek at the first  element in  the Queue")     
  log ("Peeking at the first element: ", q.peek());


  log("Remove 3 elements from the Queue")
  log ("First element: ", q.remove());
  log(q.toString());
  log ("Second element: ", q.remove());
  log(q.toString());
  log ("Third element: ", q.remove());
  log(q.toString());

  s := q.size();
  log ("Queue size: ", s)
  if (s == 0) {
    setverdict(pass);
  } else {
    setverdict(fail);
  }

  //Performance   
  log("Add ", tsp_index+1 ," elements to the queue");
  clock.start; log (clock);               
  for  (var integer index:=0; index<=tsp_index;  index:= index+1)        
  {  uc0 :=UniversalCharstring.create("Element  #: "&int2str(index) )
    q.add(uc0);
  } 
  log (clock);     clock.stop       
  // log(q.toString());
  s := q.size();
  log ("Queue size: ", s)
  if (s == 1000000) {
    setverdict(pass);
  } else {
    setverdict(fail);
  }

  //We can traverse the queue elements using an iterator.          
  //traversing the queue with an iterator      
  //iteration starts at head end!!!!! 

  var  QueueForwardIterator iterator := q.iterator() => QueueForwardIterator;
  clock.start; log (clock);     
  while(iterator.hasNext()  ){
    uc2 := iterator.next() => UniversalCharstring;
    if (uc2.toString()=="Element  #: 666") { log ("Element 666 found!!!") }  
    //log ("------>",uc2.toString())
  }
  log (clock);     clock.stop;       

  log("Remove ",  tsp_index+1 ," elements from the queue");
  clock.start; log (clock);              
  for  (var integer index:=0; index<=tsp_index; index:= index+1)        
  {  
    q.remove();
  }   
  log (clock);     clock.stop;        
  log(q.toString());
  s := q.size();
  log ("Queue size: ", s)
  if (s == 0) {
    setverdict(pass);
  } else {
    setverdict(fail);
  }


}//end testcase


control {

  execute(tcQueue());
}



}
