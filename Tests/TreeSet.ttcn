/******************************************************************************
* Copyright (c) 2000-2025 Ericsson Telecom AB
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
*
* Contributors:
*   Lenard, Nagy – initial implementation
*   Elemer, Lelik
*
******************************************************************************/
module TreeSet {

import from TTCN3TreeSet all;
import from OODataTypes all;
import from TTCN3_standard_collections all;

modulepar {

  integer tsp_index:=10000;

}

type component GeneralComp {
}


testcase tcTreeSet() runs on GeneralComp {
  var Integer zero, one, two ,three, four , five, six, seven , eight,nine , ten, eleven, fourninenine
  var integer val
  timer clock:=3600.0;

  log("Create empty TreeSet");
  var TitanTreeSet treeSet := TitanTreeSet.create();

  log (treeSet.toString());

  //try to remove from empty treeset
  treeSet.remove(Integer.create(0));

  log("Add few numbers to TreeSet");

  var integer numOfElements := 10;
  for (var integer i := 0; i < numOfElements; i := i+1) {
    var Integer toAdd;
    if (i mod 2 == 0) {
      toAdd := Integer.create(i);
    } else {
      toAdd := Integer.create(numOfElements - i);
    }

    if (treeSet.add(toAdd)) {
      log("Added ", toAdd);
      setverdict(pass);
    } else {
      setverdict(fail);
    }
  }
  log(treeSet.toString());

  log("TreeSet size check after adding elements");
  var integer s := treeSet.size();
  if (s == numOfElements) {
    log("TreeSet size correct: ", s);
    setverdict(pass);
  } else {
    log("TreeSet size incorrect: ", s);
    setverdict(fail);
  }

  log("TreeSet should contain 4");
  four := Integer.create(4);
  if (treeSet.contains(four)) {
    log("TreeSet contains 4");
    setverdict(pass);
  } else {
    log("TreeSet should contain 4");
    setverdict(fail);
  }

  log("TreeSet should not contain 11");
  eleven := Integer.create(11);
  if (not treeSet.contains(eleven)) {
    log("TreeSet does not contain 11");
    setverdict(pass);
  } else {
    log("TreeSet should NOT contain 11");
    setverdict(fail);
  }

  log("--------------->", treeSet)

  log("Iterate through the TreeSet")
  var Iterator tsIterator := treeSet.iterator();
  while(tsIterator.hasNext()) {
    log(tsIterator.next());
  }
  setverdict(pass);

  log("Remove 6 from the TreeSet (has 0 child => leaf node");
  six := Integer.create(6);
  treeSet.remove(six);
  log(treeSet.toString());
  if (not treeSet.contains(six)) {
    log("TreeSet does not contain 6");
    setverdict(pass);
  } else {
    log("TreeSet should NOT contain 6");
    setverdict(fail);
  }

  log("Remove 0 from the TreeSet (has 1 child: 9 and it is the head)");
  zero := Integer.create(0);
  treeSet.remove(zero);
  log(treeSet.toString());
  if (not treeSet.contains(zero)) {
    log("TreeSet does not contain 0");
    setverdict(pass);
  } else {
    log("TreeSet should NOT contain 0");
    setverdict(fail);
  }

  log("Remove 7 from the TreeSet (has 2 children: 3 and 4)");
  seven := Integer.create(7);
  treeSet.remove(seven);
  log(treeSet.toString());
  if (not treeSet.contains(seven)) {
    log("TreeSet does not contain 7");
    setverdict(pass);
  } else {
    log("TreeSet should NOT contain 7");
    setverdict(fail);
  }


  //**************************************************************************   
  clock.start; 
  log(tsp_index, "  additions")     
  for (var integer i := 0; i < tsp_index; i := i+1) {
    var Integer toAdd;
    if (i mod 2 == 0) {
      toAdd := Integer.create(i); 

    } else {
      toAdd := Integer.create(tsp_index - i);
    }

    treeSet.add(toAdd); 

  }   
  log (clock);     clock.stop;    
  //      log(treeSet.toString()); 
  log ("treeSet size:   ", treeSet.size())        


  //**************************************************************************   
  clock.start;  
  log(tsp_index, "  searches") 
  fourninenine := Integer.create(499)
  for (var integer i := 0; i < tsp_index; i := i+1) {
    treeSet.contains(fourninenine); 
  }   
  log (clock);     clock.stop;   

  //**************************************************************************   
  clock.start; 
  log(tsp_index, "  removals")      
  for (var integer i := 0; i < tsp_index; i := i+1) {
    treeSet.remove(Integer.create(i)); 
  }   
  log (clock);     clock.stop;    
  log(treeSet.toString()); 
  log ("treeSet size:   ", treeSet.size())       

  //**************************************************************************   
  //Let's populate the treeset with characterstrings
  clock.start;   
  for (var integer i := 0; i < 100; i := i+1) {
    //   float2int(int2float(upperbound - lowerbound +1)*rnd()) + lowerbound 
    val := float2int(int2float(100 -1)*rnd());
    treeSet.add(UniversalCharstring.create(int2str(val))); 
  }   
  log (clock);     clock.stop;    
  //   log(treeSet.toString()); 
  log ("treeSet size:   ", treeSet.size())     


  log("Iterate through the TreeSet and log all nodes")
  tsIterator := treeSet.iterator();
  var UniversalCharstring uc1  
  while(tsIterator.hasNext()) {
    uc1 := tsIterator.next() => UniversalCharstring
    log(uc1.toString());
    // treeSet.remove(tsIterator.next())


  }
  log(treeSet.toString());
  log(treeSet.size())





}//end test case


control {

  execute(tcTreeSet());
}

}//end module
