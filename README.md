# titan.ObjectOriented.StandardCollections

Main project page:

https://projects.eclipse.org/projects/tools.titan

The source code of the TTCN-3 compiler and executor:

https://gitlab.eclipse.org/eclipse/titan/titan.core

The Standard Collections for TTCN3 Object-Oriented Features is defined in Annex B of https://www.etsi.org/deliver/etsi_es/203700_203799/203790/01.02.01_60/es_203790v010201p.pdf

To enable the Object-Oriented features in the Titan compiler use the "-k" flag.
